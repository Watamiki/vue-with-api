import App from "../src/App.vue";
import { mount } from "@vue/test-utils";
import axios from "axios";

jest.mock("axios");
const apiKey = "ijtydX4cdGvOkFMvAHoBdg71yYfPhy1973NYhXfJ";

describe("API Tests", () => {
  test("makes a GET request to fetch prefectures data", async () => {
    const pferecturesMock = [{ prefCode: 1, prefName: "北海道" }];
    axios.get.mockResolvedValue({ data: { result: pferecturesMock } });
    const wrapper = mount(App);
    await wrapper.vm.$nextTick();
    expect(axios.get).toHaveBeenCalledWith(
      "https://opendata.resas-portal.go.jp/api/v1/prefectures",
      {
        headers: {
          "X-API-KEY": apiKey,
        },
      }
    );
  });
  test("handles errors when fetching prefectures", async () => {
    axios.get.mockRejectedValue(new Error("Network Error"));
    const wrapper = mount(App);
    await wrapper.vm.$nextTick();
    expect(await wrapper.vm.error).toBeDefined();
    expect(await wrapper.vm.error).toContain("Network Error");
  });
});
