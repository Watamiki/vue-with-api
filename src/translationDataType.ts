export const translationDataType: { [key: string]: string } = {
  総人口: "General Population",
  年少人口: "Young Population",
  生産年齢人口: "Working Age Population",
  老年人口: "Elderly Population",
};
