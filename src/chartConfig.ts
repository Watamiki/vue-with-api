export const data = {
  labels: [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ],
  datasets: [
    {
      label: "Data One",
      backgroundColor: "rgba(255, 99, 132, 0.2)",
      borderColor: "rgba(255, 99, 132, 1)",
      borderWidth: 1,
      data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11],
    },
  ],
  chartOptions: {
    scales: {
      y: {
        beginAtZero: true,
      },
    },
    responsive: true,
    maintainAspectRatio: false,
  },
};

export const options = {
  // scales: {
  //   y: {
  //     beginAtZero: true,
  //   },
  // },
  responsive: true,
  maintainAspectRatio: false,
};

// export const data = {
//   labels: [
//     "January",
//     "February",
//     "March",
//     "April",
//     "May",
//     "June",
//     "July",
//     "August",
//     "September",
//     "October",
//     "November",
//     "December",
//   ],
//   datasets: [
//     {
//       label: "Data One",
//       backgroundColor: "#f87979",
//       data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11],
//     },
//   ],
// };

// export const options = {
//   responsive: true,
//   maintainAspectRatio: false,
// };
