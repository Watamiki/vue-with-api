# SPA to display a graph of total population by prefecture

## Environment

- vite 4.4.5
- node v19.0.0
- vue v3.3.4
- typescript v5.0.2

## Requirements

- Obtain API from "List of Prefectures (Japan)" of [RESAS (Regional Economic Analysis System) API](https://resas.go.jp/)
- Dynamically generate a checkbox for the list of prefectures from the API response
- Prefectures are translated into English
- When a check box is checked for a prefecture, the "population composition" of the selected prefecture is obtained from the RESAS API.
- Dynamically generate and display a line graph with X-axis: year, Y-axis: number of people from the Population Composition API response.
- The list of prefectures and total population information uses data from the RESAS API.
- Graphs are drawn using a graph library (Chart.js)

## Image

![Screenshot_2023-10-05_at_12.18.13](/uploads/516aa1bf61bfc9f9b65ebd62b6fc06fe/Screenshot_2023-10-05_at_12.18.13.png)

# Vue 3 + TypeScript + Vite

This template should help get you started developing with Vue 3 and TypeScript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support For `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
   1. Run `Extensions: Show Built-in Extensions` from VSCode's command palette
   2. Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.
